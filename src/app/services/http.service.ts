import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin, Observable, Subscription } from 'rxjs';
import { Movie, MovieDetails } from '../models/app.models';

interface HttpServiceInterface {
  findMovies(name: string): Observable<Movie>;
}

@Injectable({
  providedIn: 'root',
})
export class HttpService implements HttpServiceInterface {
  constructor(private http: HttpClient) {}

  public findMovies(name: string): Observable<Movie> {
    return this.http.get<Movie>('https://imdb8.p.rapidapi.com/title/find', {
      params: {
        q: name,
      },
    });
  }

  public getMovieDetails(id: string): Promise<MovieDetails> {
    return this.http
      .get<{ image: { url: string }; title: string; year: number }>(
        'https://imdb8.p.rapidapi.com/title/get-details',
        {
          params: {
            tconst: id,
          },
        }
      )
      .toPromise()
      .then(
        (details: { image: { url: string }; title: string; year: number }) => {
          return this.http
            .get<{ plots: { text: string }[] }>(
              'https://imdb8.p.rapidapi.com/title/get-plots',
              {
                params: {
                  tconst: id,
                },
              }
            )
            .toPromise()
            .then((plot) =>
              this.http
                .get<{ rating: number }>(
                  'https://imdb8.p.rapidapi.com/title/get-ratings',
                  {
                    params: {
                      tconst: id,
                    },
                  }
                )
                .toPromise()
                .then((rating) => ({
                  plot: plot.plots[0].text,
                  id: id,
                  image: details.image.url,
                  title: details.title,
                  year: details.year,
                  ratings: rating.rating,
                }))
            );
        }
      );
  }
}

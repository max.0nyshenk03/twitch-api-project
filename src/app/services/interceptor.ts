import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class httpInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const modified = req.clone({
      setHeaders: {
        'x-rapidapi-key': 'a587a7f2femshaeca01f6a290159p19d60bjsn5754d9a13396',
        'x-rapidapi-host': 'imdb8.p.rapidapi.com',
        useQueryString: 'true',
      },
    });
    return next.handle(modified);
  }
}

import { Component, OnInit } from '@angular/core';

import { MovieDetails } from '../models/app.models';
import { HttpService } from '../services/http.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'movie-details',
  templateUrl: './movieDetails.component.html',
  styleUrls: ['./movieDetails.component.scss'],
})
export class MovieDetailsComponent implements OnInit {
  loading = false;
  movie: MovieDetails | null = null;
  constructor(private http: HttpService, private route: ActivatedRoute) {}

  getDetails(id: string): void {
    this.loading = true;
    this.http.getMovieDetails(id).then((movie) => {
      this.movie = movie;
      this.loading = false;
    });
  }

  ngOnInit() {
    this.getDetails(this.route.snapshot.params.id);
  }
}

export type Movie = {
  id: string;
  image: string;
  actors: string[];
  title: string;
  year: number;
};
export type MovieDetails = {
  id: string;
  image: string;
  plot: string;
  title: string;
  year: number;
  ratings: number;
};

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpService } from './services/http.service';
import { httpInterceptor } from './services/interceptor';
import { MovieDetailsComponent } from './movieDetails/movieDetails.component';
import { IndexComponent } from './index.component';

@NgModule({
  declarations: [IndexComponent, AppComponent, MovieDetailsComponent],
  imports: [BrowserModule, HttpClientModule, AppRoutingModule],
  providers: [
    HttpService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: httpInterceptor,
      multi: true,
    },
  ],
  bootstrap: [IndexComponent],
})
export class AppModule {}

import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { MovieDetailsComponent } from './movieDetails/movieDetails.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

const appRoutes: Routes = [
  { path: '', component: AppComponent },
  { path: 'details/:id', component: MovieDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

export default appRoutes;

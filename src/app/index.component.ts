import { Component } from '@angular/core';

@Component({
  selector: 'index-root',
  template: ` <router-outlet></router-outlet> `,
})
export class IndexComponent {
  constructor() {}
}

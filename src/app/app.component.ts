import { Component } from '@angular/core';
import { Inject } from '@angular/core';

import { HttpService } from './services/http.service';
import { Movie } from './models/app.models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'twitch-app';
  movies: Movie[] | null = null;
  loading = false;
  movieListOn = false;

  constructor(@Inject(HttpService) public http: HttpService) {}

  searchMovie(event: Event): void {
    event.preventDefault();
    this.loading = true;
    this.movieListOn = true;
    this.http
      .findMovies((event.target as HTMLInputElement).value)
      .subscribe((body: any) => {
        this.movies = body.results
          .map(
            (i: {
              id: string;
              image?: {
                url: string;
              };
              principals?: {
                name: string;
              }[];
              year: number;
              title: string;
            }): Movie => ({
              id: i.id.split('/').filter((i: string) => i !== '')[1],
              image: i.image?.url || '',
              actors: i.principals?.map((p: any) => p.name) || [],
              year: i.year,
              title: i.title,
            })
          )
          .filter((m: Movie) => !!m.title);
        this.loading = false;
      });
  }
}
